package org.amdatu.chat.rooms.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.amdatu.chat.rooms.storage.Room;
import org.amdatu.chat.rooms.storage.RoomsStorageService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Path("rooms")
@Component(provides=Object.class)
public class RoomsResource {

	@ServiceDependency
	private volatile RoomsStorageService m_storage;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Room> list() {
		return m_storage.list();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public void create(Room room) {
		m_storage.add(room);
	}
}
