package org.amdatu.chat.rooms.storage.mem;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.amdatu.chat.rooms.storage.Room;
import org.amdatu.chat.rooms.storage.RoomsStorageService;
import org.apache.felix.dm.annotation.api.Component;

@Component
public class MemoryRoomStorageService implements RoomsStorageService {

	private final List<Room> m_rooms = new CopyOnWriteArrayList<>();
	
	public MemoryRoomStorageService() {
		m_rooms.add(new Room("JavaRoom"));
	}
	
	@Override
	public void add(Room room) {
		m_rooms.add(room);
	}

	@Override
	public List<Room> list() {
		return m_rooms;
	}

}
