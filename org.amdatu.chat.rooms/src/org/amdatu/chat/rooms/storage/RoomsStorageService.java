package org.amdatu.chat.rooms.storage;

import java.util.List;

public interface RoomsStorageService {
	void add(Room room);
	List<Room> list();
}
