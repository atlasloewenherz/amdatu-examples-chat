#
# Script to load a collection of artifacts into Apache ACE.
#

echo "Loading artifacts into Apache ACE"

dist = getproperty distribution
src = getproperty source
project = ((new File "") absoluteFile) parentFile

echo "Distribution: $dist"
echo "Source: $src"

#
# Setup the repositories.
#
sourceindex = (repo:index $project/$src)

releaserepo = (repo:repo R5 file:$project/../../cnf/localrepo/index.xml)
sourcerepo = (repo:repo R5 $sourceindex)
targetrepo = (repo:repo OBR http://localhost:8080/obr/repository.xml)

#
# Deploy artifacts from the source repository to the target repository,
# baselined against the release repository.
#
deployed = (repo:cd $releaserepo $sourcerepo $targetrepo)

#
# Create a new workspace in the ACE client and first delete all
# artifacts, features, distributions and their relations (only
# things related to this distribution). Then create new ones based
# on all the artifacts we just deployed.
#
workspace = (ace:cw)

if { (coll:first ($workspace lrp "(&(Bundle-SymbolicName=org.apache.felix.deployment.rp.autoconf)(Bundle-Version=0.1.5))")) } {
	echo "Resource processor already installed"
} {
	echo "Installing resource processor"
	$workspace ca file:$project/ace-libs/org.apache.felix.deployment.rp.autoconf-0.1.5.jar true
}

a2f = ($workspace la2f "(rightEndpoint=\(name=$dist\))")
each $a2f {
	echo "deleting $it"
	$workspace da2f $it
}
f = ($workspace lf "(name=$dist)")
each $f {
	echo "deleting $it"
	$workspace df $it
}
f2d = ($workspace lf2d "(rightEndpoint=\(name=$dist\))")
each $f2d {
	echo "deleting $it"
	$workspace df2d $it
}
d = ($workspace ld "(name=$dist)")
each $d {
	echo "deleting $it"
	$workspace dd $it
}
d2t = ($workspace ld2t "(leftEndpoint=\(name=$dist\))")
each $d2t {
	echo "deleting $it"
	$workspace dd2t $it
}
each $deployed {
	identity = $it getIdentity
	version = $it getVersion
	name = "$identity-$version"
	url = $it getUrl
	mimetype = $it getMimetype
	if {"application/vnd.osgi.bundle" equals $mimetype} {
		if { (coll:first ($workspace la "(&(Bundle-SymbolicName=$identity)(Bundle-Version=$version))")) } {
			echo "Skipping existing bundle $name"
		} {
			echo "Creating bundle $name"
			$workspace ca [
				Bundle-SymbolicName = "$identity"
				Bundle-Version = "$version"
				artifactName = "$name"
				url = "$url"
				mimetype = "$mimetype"
			]
		}
		$workspace ca2f "(&(Bundle-SymbolicName=$identity)(Bundle-Version=$version))" "(name=$dist)" "N" "1"
	}
	if {"application/xml:osgi-autoconf" equals $mimetype} {
		if { (coll:first ($workspace la "(&(Bundle-SymbolicName=$identity)(Bundle-Version=$version))")) } {
			echo "Skipping existing configuration $name"
		} {
			echo "Creating configuration $name"
			$workspace ca [
				artifactName = "$name"
				filename = "$name"
				url = "$url"
				mimetype = "$mimetype"
				processorPid = "org.osgi.deployment.rp.autoconf"
			]
		}
		$workspace ca2f "(filename=$name)" "(name=$dist)" "N" "1"
	}
}
$workspace cf "$dist"
$workspace cf2d "(name=$dist)" "(name=$dist)"
$workspace cd "$dist"
$workspace cd2t "(name=$dist)" "(distribution=$dist)"

$workspace commit

echo "Done"

misc:sleep 2000
exit 0
