package org.amdatu.chat.wordcounter;

import java.util.List;

public interface WordCounter {

	String getMostPopular(List<String> lines);

}